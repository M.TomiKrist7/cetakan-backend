import express from "express";
import jenis from "../models/jenis.js";

const app = express.Router();


app.get('/', async (req,res)=>{
    const listJenis = await jenis.find()

    if(!listJenis){
        return res.status(500).send('Tidak ada data')
    }
    return res.status(200).send(listJenis);
})

app.get('/status/:para', async(req,res)=>{
    const getType = await jenis.find({status:req.params.para})
    if(!getType){
        return res.status(500).send("tidak ada Admin");
    }
    return res.status(201).send(getType);
})


app.post(`/`, async(req,res) =>{
    
             const Jenis = new jenis ({
             deskripsi: req.body.deskripsi,
             type: req.body.tipe,
             note: req.body.note,
         })
 
         Jenis.save().then((Jenis => {
             return res.status(201). json({
             
                 success:true,
                 message: "berhasil membuat baru",
                
             }) 
         })).catch(err=>{
            return res.status(500).json({ 
            
                success:false,
                message: "gagal membuat jenis!",
                
            })
         })
         // return res.status("json Okay").json({User})
        
     
 })


export default app