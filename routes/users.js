import express, { response } from "express";
import user from "../models/user.js";
import bcrypt from "bcrypt";
import  jwt  from "jsonwebtoken";



const app = express.Router();

app.get('/', async(req,res)=>{
    const listUsers = await user.find().select('-passwordHash');

    if(!listUsers){ 
        res.status(500).json({success:false});
    }

    res.send(listUsers);

})

// app.get('/admin/:para', async(req,res)=>{
//     const getAdmin = await user.find({isAdmin:req.params.para}).select('name')
//     if(!getAdmin){
//         return res.status(500).send("tidak ada Admin");
//     }
//     return res.status(201).send(getAdmin);
// })




app.get('/:id', async(req,res)=>{
    const getUser = await user.findById(req.params.id).select('-passwordHash');

    if(!getUser){
        return res.status(500).send("User tidak ditemukan")
    }
    return res.status(201).send(getUser);
})





app.post(`/daftar`, async(req,res) =>{
    
   const checkNewUser = await user.findOne({email: req.body.email})
    
   
    
    if(!checkNewUser){
            const User = new user({
            name: req.body.name,
            full_name: req.body.full_name,
            email: req.body.email,
            passwordHash : await bcrypt.hash(req.body.password, 10) ,
            phone: req.body.phone,
            isAdmin: req.body.isAdmin,
            street: req.body.street,
            apartment: req.body.apartment,
            zip: req.body.zip,
            city: req.body.city,
            country: req.body.country,
           
        })

        User.save().then((User => {
            return res.status(201). json({
            
                success:true,
                message: "berhasil membuat baru",
               
            }) 
        }))
        // return res.status("json Okay").json({User})
       
    }else{

        return res.status(500).json({
            
            success:false,
            message: "Nama Perusahaan sudah terdaftar!",
           
        }) 
    }
})


app.post('/login',async(req,res)=>{

    const User = await user.findOne({email:req.body.email})
    const secret =process.env.SECRET;

    if(!User){
        return res.status(400).send('Pengguna tidak ditemukan');
    }

    if(User && bcrypt.compareSync(req.body.password,User.passwordHash)){
       const token = jwt.sign({
        userId: User.id,
        isAdmin: User.isAdmin,
       },secret,
       {expiresIn:'2h'})
       
        return res.status(200).send({user:User.email, token:token, success: true})
    }
    return res.status(201).json({success:false})

    // return res.status(200).send(User);

})

app.put('/:id', async(req,res)=>{

    const userExist = await user.findById(req.params.id);
    let newPassword

    if(req.body.password){
        newPassword = bcrypt.hash(req.body.password, 10)
    } else {
        newPassword =userExist.passwordHash;
    }

    const User = await user.findByIdAndUpdate(
        req.params.id,
        {
            name: req.body.name,
            full_name: req.body.full_name,
            email: req.body.email,
            passwordHash : newPassword ,
            phone: req.body.phone,
            isAdmin: req.body.isAdmin,
            street: req.body.street,
            apartment: req.body.apartment,
            zip: req.body.zip,
            city: req.body.city,
            country: req.body.country,

        }, {new:true}
    )
    if(!User) return res.status(500).send('tidak bisa mengupdate data');
    
    return res.send(User);
})


app.delete('/:id',(req,res)=>{
    user.findByIdAndRemove(req.params.id).then(user=>{

        if(user){
            return res.status(200).json({success:true, message:'data pengguna telah dihapus!'})
        }else{
            return res.status(404).json({success:false, message:'data pengguna tidak ditemukan'});
        }
    }).catch(err=>{
        return res.status(400).json({success: false, error: err})
    })
})




export default app;