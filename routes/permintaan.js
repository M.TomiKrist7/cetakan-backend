import  express, { response }  from "express";
import permintaan from "../models/permintaan.js";
import user from "../models/user.js"


const app = express.Router();


app.get('/',async (req,res)=>{
    const listPermintaan = await permintaan.find()
    .populate({path:'customer'})
    .populate({path:'requestor'})
    .exec();

    if(!listPermintaan){
        return res.status(500).send('Tidak ada data')
    }
    return res.status(200).send(listPermintaan);
})

app.get('/status/:para', async(req,res)=>{
    const getStatus = await permintaan.find({status:req.params.para})
    if(!getStatus){
        return res.status(500).send("tidak ada Admin");
    }
    return res.status(201).send(getStatus);
})

app.get('/pembuat/:para', async(req,res)=>{
    const getStatus = await permintaan.find({pembuat:req.params.para})
    if(!getStatus){
        return res.status(500).send("tidak ada Admin");
    }
    return res.status(201).send(getStatus);
})

app.get('/laporan/:para', async(req,res)=>{
    const getStatus = await permintaan.find({laporanFlag:req.params.para})
    if(!getStatus){
        return res.status(500).send("tidak ada Admin");
    }
    return res.status(201).send(getStatus);
})

app.get('/:id', async(req,res)=>{
    const getPermintaan = await permintaan.findById(req.params.id)
    .populate({path:'customer'})
    .populate({path:'requestor'})
    .exec();
 

    if(!getPermintaan){
        return res.status(500).send("Customer tidak ditemukan")
    }
    return res.status(201).send(getPermintaan);
})

app.post('/', async(req,res)=>{

    //  const Requestor = await user.findById(req.body.requestor)

    // if(!Requestor){

    // }
     const Permintaan = await new permintaan({
        deskripsi: req.body.deskripsi,
        produk: req.body.produk,
        paperType: req.body.paperType,
        receivedDate : req.body.receivedDate, 
        deadline: req.body.deadline,
        dbName: req.body.dbName,
        menuName: req.body.menuName,
        note: req.body.note,
        requestor: req.body.requestor,
        url: req.body.url,
        perbaikanUrl: req.body.perbaikanUrl,
        customer: req.body.customer,
        typeQuotation: req.body.typeQuotation,
        priority: req.body.priority,
        finishDate: req.body.finishDate,
        status: req.body.status,
        pembuat: req.body.pembuat,

     }) 
     
     Permintaan.save().then((permintaan =>{
       return res.status(201).json({
            success:true,
            message: "Permintaan berhasil Dibuat!",
        })
     })).catch(err=>{
        return res.status(500).json({
                
            success:false,
            message: "Permintaan gagal terdaftar!",
            
        })

     })
})

app.put('/:id', async(req,res)=>{


    const Permintaan = await permintaan.findByIdAndUpdate(
        req.params.id,
        {
            deskripsi: req.body.deskripsi,
            produk: req.body.produk,
            paperType: req.body.paperType,
            receivedDate : req.body.receivedDate, 
            deadline: req.body.deadline,
            dbName: req.body.dbName,
            menuName: req.body.menuName,
            note: req.body.note,
            requestor: req.body.requestor,
            url: req.body.url,
            perbaikanUrl: req.body.perbaikanUrl,
            customer: req.body.customer,
            typeQuotation: req.body.laporanFlag,
            priority: req.body.priority,
            finishDate: req.body.finishDate,
            status: req.body.status,
            pembuat: req.body.pembuat,

        }, {new:true}
    )
    if(!Permintaan) return res.status(500).send('tidak bisa mengupdate data');
    
    return res.send(Permintaan);
})

app.delete('/:id',(req,res)=>{
    permintaan.findByIdAndRemove(req.params.id).then(permintaan=>{

        if(permintaan){
            return res.status(200).json({success:true, message:'data pengguna telah dihapus!'})
        }else{
            return res.status(404).json({success:false, message:'data pengguna tidak ditemukan'});
        }
    }).catch(err=>{
        return res.status(400).json({success: false, error: err})
    })
})





export default app