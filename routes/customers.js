import  express from 'express'
import customer from '../models/customer.js'

const app = express.Router();

app.get('/', async(req,res)=>{
    const listCustomers =  await customer.find();

    if(!listCustomers){
        return res.status(500).send('Tidak ada data')
    }
    return res.status(200).send(listCustomers);
})

app.get('/tipe/:para', async(req,res)=>{
    const getType = await customer.find({tipe:req.params.para})
    if(!getType){
        return res.status(500).send("tidak ada Admin");
    }
    return res.status(201).send(getType);
})

app.get('/:id', async(req,res)=>{
    const getCustomer = await customer.findById(req.params.id);
    if(!getCustomer){
        return res.status(500).send("Customer tidak ditemukan")
    }
    return res.status(201).send(getCustomer);
})


app.post('/', async(req,res)=>{
    
    const customerExist = await customer.findOne({namaPerusahaan:req.body.namaPerusahaan});

    if(!customerExist){
        
        const Customer = new customer({
            nama: req.body.nama,
            namaPerusahaan: req.body.namaPerusahaan,
            tipe: req.body.tipe,
            marketing: req.body.marketing,
            note: req.body.note,
            lokasi:[{
                kota: req.body.kota,
                alamat: req.body.alamat,
                phone1: req.body.phone,
                phone2: req.body.phone1,
                phone3: req.body.phone2,
            }]
           
        })

        Customer.save().then((Customer=>{
            res.status(201).json({
                success:true,
                message: "Perusahaan berhasil mendaftar!",
            })
        }))
    } else {

        res.status(500).json({
            
            success:false,
            message: "Nama Perusahaan sudah terdaftar!",
            
        }) 

    }
    
   
})


app.put('/:id', async(req,res)=>{

    const Customer = await customer.findByIdAndUpdate(
        req.params.id,
        {
            nama: req.body.nama,
            namaPerusahaan: req.body.namaPerusahaan,
            tipe: req.body.tipe,
            marketing: req.body.marketing,
            note: req.body.note,
            lokasi:[{
                kota: req.body.kota,
                alamat: req.body.alamat,
                phone1: req.body.phone,
                phone2: req.body.phone1,
                phone3: req.body.phone2,
            }]

        }, {new:true}
    )
    if(!Customer) return res.status(500).send('tidak bisa mengupdate data');
    
    return res.send(Customer);
})


app.delete('/:id',(req,res)=>{
    customer.findByIdAndRemove(req.params.id).then(customer=>{

        if(customer){
            return res.status(200).json({success:true, message:'data customer telah dihapus!'})
        }else{
            return res.status(404).json({success:false, message:'data customer tidak ditemukan'});
        }
    }).catch(err=>{
        return res.status(400).json({success: false, error: err})
    })
})



export default app