import mongoose from "mongoose";


const jenisSchema = mongoose.Schema({
    deskripsi:{
        type: String,
        required: true,
    },
    type:{
        type: String,
        default:''  
    },
    note:{
        type: String,
        default:''  
    },
})

const jenis = mongoose.model('jenis', jenisSchema)

export default jenis