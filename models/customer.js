import mongoose from "mongoose";

const customerSchema = mongoose.Schema({
    nama:{
        type: String,
        required: true,
    },
    namaPerusahaan:{
        type: String,
        required: true,
    },
    tipe:{
        type: String,
        required: true,
        lowercase: true,
        trim: true,
    },
    marketing:{
        type: String,
        required: true,
    },
    note:{
        type: String,
        default:''
        
    },

    lokasi:[ {
            kota:{
                type: String,
                required: true, 
            },
            alamat:{
                type: String,
                required: true,
            },
            phone1:{
                type: String,
                default:''
            },
            phone2:{
                type: String,
                default:''
            },
            phone3:{
                type: String,
                default:''
            },
    }],
        
    
})

const customer = mongoose.model('customer', customerSchema);

export default customer;