import mongoose from "mongoose";

// mongoose.set('debug',true);

const permintaanSchema = mongoose.Schema({
    deskripsi:{
        type: String,
        required: true,
    },
    produk: {
        type: String,
        required: true,
    },
    paperType:{
        type: String,
        required: true,
    },
    receivedDate:{
        type: Date,
        required: true,
    },
    deadline : {
        type : Date,
        required: true,
    } ,

    dbName : {
        type : String,
        default:""
        
    } ,

    menuName : {
        type : String,
        
    } ,

    note : {
        type : String,
        
    } ,

    requestor : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: true
        
    } ,

    url : {
        type : String,
        work: mongoose.SchemaTypes.Url,
        profile: mongoose.SchemaTypes.Url,
        default: ''
    } ,

    perbaikanUrl : {
        type : String,
        default: ''
    } ,

    customer : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'customer',
        required: true
        
    } ,
    laporanFlag :{
        type: String,
        default: 'N',
    },

    typeQutation : {
        type: String,
        default: ''
    },

    priority : {
        type: String,
        default: ''
    },

    finishDate : {
        type: Date,
        default: ''
    },

    status : {
        type: String,
        default: ''
    },

    pembuat : {
        type: String, /* mongoose.Schema.Types.ObjectId,
        ref: 'user',*/
        default:''
    },

})

permintaanSchema.virtual('id').get(function(){
    return this._id.toHexString();
})


permintaanSchema.set('toJSON',{
    virtual:true,
})

const permintaan = mongoose.model('permintaan',permintaanSchema);
 
export default permintaan;