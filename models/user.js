import mongoose from "mongoose";
import bcrypt from "bcrypt";

// mongoose.set('debug',true);

const length = 10;


const userSchema = mongoose.Schema({
    name:{
        type: String,
        required: true,
    },
    full_name: {
        type: String,
    },
    email:{
        type: String,
        required: true,
        unique: true ,
        typeof: mongoose.SchemaTypes.Email,
        lowercase: true,
        trim: true,
    },
    passwordHash:{
        type: String,
        required: true,
        bcrypt: true,
    },
    phone : {
        type : String,
        
    } ,
    isAdmin :{
        type: String,
        length:1,
        default: "N",

    },
    street : {
        type: Boolean,
        default: false
    },
    apartment : {
        type: String,
        default: ''
    },
    zip : {
        type: String,
        default: ''
    },
    city : {
        type: String,
        default: ''
    },
    country : {
        type: String,
        default: ''
    },
})


// async function isThisEmailInUse (email){

// const user =await this.findOne({email})

//         if(user) return false

//         return true

// }




const user = mongoose.model('users',userSchema);

export default user;

