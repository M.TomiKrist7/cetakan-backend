import express from 'express';
import morgan from 'morgan';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import cors from 'cors';
import bodyParser from 'body-parser';


dotenv.config({path:".env"});


const app = express()
const mongoString = process.env.DATABASE_URL
const namaDB = process.env.DBNAME
const api = process.env.API_URL

app.use(bodyParser.json());

// app.use(express.urlencoded({extended:true}));

app.listen(3000,()=>{
    console.log(`Server Started at ${3000}`)
})

mongoose.connect(mongoString,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    dbName: namaDB
}) 
    .then (()=> console.log('Db Connect'))
    .catch(err=> console.log(err))


import usersRouter from './routes/users.js';
import customersRouter from './routes/customers.js'
import permintaanRouter from './routes/permintaan.js'
import jenisRouter from './routes/jenis.js'
import auth from './helpers/auth.js'
import errorHandler from './helpers/errorHandler.js'

//middleware
app.use(cors());
app.options('*',cors());
app.use(morgan('tiny'));
// app.use(auth());
app.use(errorHandler);
app.use(`${api}/users`,usersRouter);
app.use(`${api}/customers`,customersRouter);
app.use(`${api}/permintaan`,permintaanRouter);
app.use(`${api}/jenis`,jenisRouter);

